<?php

namespace App\Http\Controllers;

use App\Photo;
use App\User;
use App\UserRoom;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Response;

class PhotoController extends Controller
{
  
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $photos = Photo::where('user_id')->get();
        return response()->json($photos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = UserRoom::where('room_id', $request->room)->first()->user_id;
        $user = User::where('id', $user_id)->first();
        $this->validate($request, [

            'filename' => 'required',
            'filename.*' => 'mimes:jpeg,png,jpg'
    
        ]);
        if($request->hasfile('filename'))
        {
            
            foreach($request->file('filename') as $file)
            {
                // $path = $file->store('public');
                // $files = explode("/", $path)[1];
                // $data[] = $files;  
                $name = $file->getClientOriginalName();
                $file->move(public_path().'/images/', $name);  
                $photo = new Photo();
                $photo->name = explode(".", $file->getClientOriginalName())[0];
                $photo->filename = $name;
                $user->createUserPhoto($photo);
           }
        }

        // $file= new Photo();
        // $file->filename=json_encode($data);
        // $file->name = 'teste';
        
        // $file->save();
        // \Session::flash('success', 'Sua(s) foto(s) foram adicionadas ao quarto com sucesso');
       return redirect()->back()->with('success', 'Your files has been successfully added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function history(Photo $photo)
    {
        return view('admin.photo.history');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function edit(Photo $photo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Photo $photo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Photo $photo)
    {
        //
    }
}
