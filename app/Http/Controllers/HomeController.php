<?php

namespace App\Http\Controllers;

use App\Room;
use App\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function home(){
        if(Auth::user()){
            $user = Auth::user();
            if($user->role == 1){
                return redirect()->action('HomeController@indexAdmin');
            }else{
                return redirect()->action('HomeController@indexGuest');
            }
        }
        else{
            return view('login');
        }
    }

    public function logout(){
        Auth::logout();
        return view('login');
    }

    public function login(Request $request){
        $credentials = ['email'  =>$request->email, 'password'=> $request->password];
        if(Auth::attempt($credentials)){
            $user = Auth::user();
            if($user->role == 1){
                return redirect()->action('HomeController@indexAdmin');
            }else{
                return redirect()->action('HomeController@indexGuest');
            }
        }else{
            return redirect()->back()->with('msg', 'Acesso negado para estas credencias');
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('welcome');
    }

    public function indexAdmin()
    {
        $rooms = Room::with('users', 'users.photos')->get();
        return view('admin.dashboard', compact('rooms'));
    }

    public function indexGuest()
    {
        $user = auth()->user();
        $photos = Photo::where('user_id', $user->id)->get();
        return view('hospede.index', compact('user', 'photos'));
    }


    public function resgister()
    {
    return view('register');
    }

    public function settings(){
        $user = Auth::user();
    return view('hospede.settings', compact('user'));
    }
}
