<?php

namespace App\Http\Controllers;

use App\User;
use App\Room;
use App\UserRoom;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('hospede.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.createUser');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        try {
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->user_email;
            $user->phone = $request->user_telefone;
            $user->role = $request->type_user;
            $user->check = 1;
            if ($request->type_user == 2) {
                $user->data_saida = $request->data_saida;
                // $user->password = null;
                if($request->confirm_password == $request->password) {
                    $user->password = bcrypt($request->password);
                }
                $user->save();
                $room = Room::find($request->roomUser_id);
                $room->attachUser($user);
                $room->status = 1;
                $room->save();
                $roomUser = UserRoom::where('user_id', $user->id)->first();
                $roomUser->token = str_random(15);
                $roomUser->save();
            } else {
                if($request->confirm_password == $request->password) {
                    $user->password = bcrypt($request->password);
                }
                $user->data_saida = null;
                $user->save();
            }

            return redirect()->back()->with('success', "Sucesso ao cadastrar um novo usuário");
        } catch (\Throwable $th) {
            // dd($th);
            return redirect()->back()->with('error', "Não foi possível ao cadastrar um novo usuário");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $User
     * @return \Illuminate\Http\Response
     */
    public function show(User $User)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $User
     * @return \Illuminate\Http\Response
     */
    public function edit(User $User)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $User
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $User)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $User
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $User)
    {
        //
    }

    public function login_hospede (Request $request)
    {
        $credentials = ['email' => $request->email_guest];
        
        if(Auth::attempt($credentials)){
            $user = Auth::user();
            if($user->role == 1){
                return redirect()->action('HomeController@indexAdmin');
            }else{
                $room = UserRoom::where('user_id', $user->id)->where('token', $request->token_room)->first()->room_id;
                return redirect()->route('guest')->with($room);
            }
        }else{
            return redirect()->back()->with('msg', 'Acesso negado para estas credencias');
        }
    }
}
