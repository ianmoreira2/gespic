<?php

namespace App;

use App\User;
use App\UserRoom;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = ['name', 'ramal', 'status'];

    public function users()
    {
        return $this->belongsToMany(User::class)->using(UserRoom::class);
    }

    public function attachUser(User $user)
    {
        $this->users()->attach($user);
    }
}
