<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room_User extends Model
{
    protected $fillable = [ 'user_id', 'room_id', 'check_out']; 

}
