<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class UserRoom extends Pivot
{
    protected $table = 'room_user';
    protected $fillable = [ 'user_id', 'room_id', 'token', 'status']; 
    
}
