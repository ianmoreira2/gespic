<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = ['user_id', 'name', 'description', 'interest', 'filename']; 

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
