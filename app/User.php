<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Room;
use App\Photo;
use App\UserRoom;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name', 'email', 'data_saida', 'phone', 'role', 'check', 'password']; 


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = [
    //     'password', 'remember_token',
    // ];

    public function rooms() {
        return $this->belongsToMany(Room::class)->using(UserRoom::class);
    }

    public function photos() {
        return $this->hasMany(Photo::class);
    }

    public function createUserPhoto(Photo $photo)
    {
        $this->photos()->save($photo);
    }

}
