<?php


Route::get('/', function () {
    return view('login');
})->name('home');

Route::post('/login', 'HomeController@login')->name('login');
Route::get('/register', 'HomeController@resgister')->name('register');
Route::post('/logout', 'HomeController@logout')->name('logout');

Route::post('/login_hospede', 'UserController@login_hospede')->name('hospede');

Route::group(['prefix' => 'admin', 'middleware'=>['auth', 'admin']], function() {
    Route::get('/', 'HomeController@indexAdmin');
    Route::resource('room', 'RoomController');
    Route::resource('user', 'UserController');
    Route::post('/photo/store', 'PhotoController@store');
    Route::get('/photo/history', 'PhotoController@history');
    Route::post('/photo/index/{id}', 'PhotoController@index');
    Route::resource('photo', 'PhotoController');
});

Route::group(['prefix' => 'guest', 'middleware'=>['auth', 'hospede']], function() {
    Route::get('/settings','HomeController@settings')->name('settings');
    Route::get('/', 'HomeController@indexGuest');

});