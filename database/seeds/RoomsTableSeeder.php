<?php

use Illuminate\Database\Seeder;
use App\Room;
use Illuminate\Support\Str;
class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Room::insert([
            'name' => Str::random(10),
            'ramal' => Str::random(10),
            'status' => '0',
        ]);
        Room::insert([
            'name' => Str::random(10),
            'ramal' => Str::random(10),
            'status' => '0',
        ]);
        Room::insert([
            'name' => Str::random(10),
            'ramal' => Str::random(10),
            'status' => '0',
        ]);
        Room::insert([
            'name' => Str::random(10),
            'ramal' => Str::random(10),
            'status' => '0',
        ]);
        // factory(Room::class, 50)->insert([
        //     'name' => Str::random(10),
        //     'ramal' => Str::random(10),
        //     'status' => 0
        // ]);
    }
}
