<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([

            'name' => 'Ian Andrade Moreira',
            'email' => 'ianmoreira80@gmail.com',
            'phone' => '(73) 981500227',
            'role' => '1',
            'check' => '1',
            'password' => bcrypt('arroz10'),
        ]);
    }
}
