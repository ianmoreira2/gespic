@extends('layouts.app_guest')
@section('title','Home')
@push('header-scripts-aux')
    <link rel="stylesheet" href="{{ asset('css/style.css')}}">

@endpush
@section('content')
<div class="user_header"></div>
<div class="user_content">
    <div class="cards row">
        @foreach ($photos as $item)
        <div class="card col-md-4 col-sm-6 col-lg-3" onclick="showModal({{ $item }})" style="cursor: pointer">
            <img src="{{ asset('images/'.$item->filename) }}" alt=""> 
        </div>
        @endforeach
    </div>
</div>
@include('hospede.show')
<script>
    function showModal(obj) {
        var html = '';
        html += `<img src="{{ asset('images/${obj.filename}') }}" style="width: 100%" />`;
        $('#photo_content').html(html);
        $('#modal-photo').modal('show');
    }
</script>
@endsection
          
           