@extends('layouts.app_guest')
@section('title','Configurações')

@section('content')
    <div class="x_content">
        <form class="form-horizontal form-label-left" role="form" method="POST" action="{{ url('/register') }}">
            {{csrf_field()}}
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nome complemento <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" class="form-control col-md-7 col-xs-12" name="name" required="required" value="{{ $user->name }}"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_name">Nome de usuário <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" class="form-control col-md-7 col-xs-12" name="user_name" required="required" value="{{ $user->user_name }}" />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="email" class="form-control col-md-7 col-xs-12" name="email"  required="required" value="{{ $user->email }}" />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone">Contato <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="phone" class="form-control col-md-7 col-xs-12" name="phone"  required="required" value="{{ $user->phone }}" />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Senha <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="password" class="form-control col-md-7 col-xs-12" name="password"  required="required" />
                </div>
            </div>
            <div class="form-group">
                 <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password_confirmation">Confirmação da senha <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="password" class="form-control col-md-7 col-xs-12" name="password_confirmation"  required="required" />
                </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <button class="btn btn-success" type="submit">Alterar</button>
                </div>
            </div>

          
            
        </form>
    </div>
@endsection
          
           