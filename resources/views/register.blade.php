@extends('layouts.auth')
@section('title','Login')
@section('content')
    <div class="animate form login_form">
        <section class="login_content">
            <form role="form" method="POST" action="{{ url('/register') }}">
                {{csrf_field()}}
                <h1>Crie sua conta</h1>
                <div class="form-group">
                    <input type="text" class="form-control" name="name" placeholder="Nome completo" required="" />
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="user_name" placeholder="Nome de usuário" required="" />
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" name="email" placeholder="Email" required="" />
                </div>
                <div class="form-group">
                        <input type="phone" class="form-control" name="phone" placeholder="Contato" required="" />
                    </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="password" placeholder="Senha" required="" />
                </div>

                <div class="form-group">
                    <input type="password" class="form-control" name="password_confirmation" placeholder="Confirmação da senha" required="" />
                </div>
                <div>
                    <button class="btn btn-default submit" type="submit">Cadastrar</button>
                    <a class="reset_pass" href="{{ url('/password/reset') }}">Esqueceu sua senha?</a>
                </div>

                <div class="clearfix"></div>

                <div class="separator">
                    <p class="change_link">Já tem uma conta?
                        <a href="{{route('login')}}" class="to_register">Cadastrar </a>
                    </p>

                    <div class="clearfix"></div>
                    <br />

                    <div><span class="glyphicon glyphicon-camera" aria-hidden="true"></span>
                        <h1><i class="fa fa-plus-circle"></i> {{config('app.name')}}</h1>
                        <p>©{{date('Y')}} </p>
                    </div>
                </div>
            </form>
        </section>
    </div>

@endsection
