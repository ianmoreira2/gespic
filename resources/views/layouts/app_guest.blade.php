<!DOCTYPE html>
<html lang="en">
@include('partials._head')

<body class="nav-md">
    <div class="container body">

        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="{{url('/guest')}}" class="site_title"><i class="fa fa-camera"></i>
                            <span>{{config('app.name')}}</span></a>
                    </div>

                    <div class="clearfix"></div>

                    <!-- menu profile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="{{  asset('images/img.jpg')}}" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Bem vindo,</span>
                            <h2>{{auth()->user()->name}}</h2>
                        </div>
                    </div>
                    <!-- /menu profile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <h3>General</h3>
                            <ul class="nav side-menu">
                                <li><a href="{{ url('guest/')}}"><i class="fa fa-home"></i> Home</a>
                                </li>
                                <li><a href="{{route('settings')}}"><i class="fa fa-cogs"></i> Configurações</a>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <!-- /sidebar menu -->

                    {{--  @include('partials._sidenav_footer')  --}}
                </div>
            </div>

            <!-- top navigation -->
            @include('partials._topnav')
            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">
                <div class="page-title">
                    <div class="title_left">
                        <h3>@yield('title')</h3>
                    </div>

                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            @yield('content')

                        </div>
                    </div>
                </div>
            </div>
            <!-- /page content -->

            <!-- footer content -->
            {{-- @include('partials._footer') --}}
            <!-- /footer content -->
        </div>
    </div>



    <script src="{{asset('js/app.js')}}"></script>
    @stack('scripts')


</body>

</html>