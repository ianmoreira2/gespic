<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
@include('partials._head')
<body class="login">
{{-- <a class="hiddenanchor" id="signup"></a>
<a class="hiddenanchor" id="signin"></a> --}}
    @yield('content')


<script src="{{asset('js/app.js')}}"></script>
@stack('scripts')

</body>
</html>
