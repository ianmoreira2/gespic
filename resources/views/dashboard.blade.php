@extends('layouts.app')
@section('title','Dashboard')
@section('content')
<div class="component">
    <h3>Dashboard</h3>
    <div id="card-room" style="display: grid; grid-template-columns: repeat(6, 1fr); grid-gap: 1rem">
        @foreach ($rooms as $item)
            <div id="card" style="width: 18rem; padding-top: 1rem; margin: 0 auto; cursor: pointer;">
                <div class="card-body" style="border: 1px solid #ccc; padding: 1.25rem; border-radius: .3rem">
                    <h5 class="card-title">{{ $item->name}}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">{{ $item->ramal }}</h6>
                    @if($item->status == 0)
                    <p class="card-link">Não à hospedes neste quarto</p>
                    @else
                    <p>Há hospedes neste quarto</p>
                    @endif
                </div>
            </div>    
        @endforeach
    </div>
</div>
    {{-- @include('partials._toptiles') --}}
@endsection
