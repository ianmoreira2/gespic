@extends('layouts.app')
@section('title','Criar Usuário')
@section('content')
<div class="component">
  <h3>Criar Usuário</h3>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        {{-- <div class="x_title"></div> --}}
        @if(Session::has('success'))
        <div class="alert alert-success">
          <p>{{ Session::get('success') }}</p>
        </div>
        @endif
        
        @if(Session::has('error'))
        <div class="alert alert-danger">
          <p>{{ Session::get('error') }}</p>
        </div>
        @endif
        <div id="error_password" class="alert alert-danger" style="display: none">
          <p>Senhas não são iguais</p>
        </div>
        <div class="x_content">
          <form action="{{ route('user.store') }}" method="POST" class="form-horizontal form-label-left">
            <input name="_token" id="token" type="hidden" value="{{ csrf_token() }}"/>
            <div class="form-group">
              <label for="" class="control-label col-md-3 col-sm-3 col-xs-12">Nome</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" class="form-control col-md-7 col-xs-12" required name="name" id="name">
              </div>
            </div>
            <div class="form-group">
              <label for="" class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="email" class="form-control col-md-7 col-xs-12" required name="user_email" id="user_email">
              </div>
            </div>
            <div class="form-group">
              <label for="" class="control-label col-md-3 col-sm-3 col-xs-12">Telefone</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="phone" class="form-control col-md-7 col-xs-12" required name="user_telefone" id="user_telefone">
              </div>
            </div>
            <div class="form-group">
              <label for="" class="control-label col-md-3 col-sm-3 col-xs-12">Tipo de usuário</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <select class="form-control" id="type_user" name="type_user">
                  <option value="">Escolha o tipo de usuário</option>
                  <option value="1">Administrador</option>
                  <option value="2">Hóspede</option>
                </select>
              </div>
            </div>
            <div class="form-group" id="form_date" style="display: none">
              <label for="" class="control-label col-md-3 col-sm-3 col-xs-12">Data de check-out</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" class="form-control col-md-7 col-xs-12" required name="data_saida" id="data_saida">
              </div>
            </div>
            <div id="choose_room" class="form-group" style="display: none">
              <label for="" class="control-label col-md-3 col-sm-3 col-xs-12">Quarto</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <select class="form-control" name="roomUser_id" id="roomUser_id">
                  <option value="">Qual o quarto que o hóspode se encontra:</option>
                  @foreach (range(1, 10) as $item)
                  <option value="{{ $item }}">Quarto {{ $item }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="" class="control-label col-md-3 col-sm-3 col-xs-12">Senha</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="password" class="form-control col-md-7 col-xs-12" required name="password" id="form_password">
              </div>
            </div>
            <div class="form-group">
              <label for="" class="control-label col-md-3 col-sm-3 col-xs-12">Confirme a senha</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="password" class="form-control col-md-7 col-xs-12" required name="confirm_password" id="form_confirm_password">
              </div>
            </div>
            {{-- <div class="ln_solid"></div> --}}
            <div class="form-group">
              <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                <button type="submit" class="btn btn-success" id="submit_button">Submit</button>
                <button type="button" class="btn btn-primary">Cancelar</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script>

  $('#type_user').change(function() {
    let valor = $(this).val()
    if (valor == '') {
      document.getElementById('choose_room').style.display = 'none'
      document.getElementById('form_date').style.display = 'none'
      document.getElementById('form_password').disabled = true
      document.getElementById('form_confirm_password').disabled = true
    }
    if (valor == 2) {
      document.getElementById('choose_room').style.display = 'block'
      document.getElementById('form_date').style.display = 'block'
      document.getElementById('form_password').disabled = false
      document.getElementById('form_confirm_password').disabled = false
    } 
    if(valor == 1) {
      document.getElementById('choose_room').style.display = 'none'
      document.getElementById('form_date').style.display = 'none'
      document.getElementById('form_password').disabled = false
      document.getElementById('form_confirm_password').disabled = false
    }
  })

  $('#confirm_password').change(function() {
    let valor = $(this).val()
    if (valor != $('#passowrd').val()) {
      document.getElementById('error_password').style.display = 'block'
      document.getElementById('submit_button').disabled = true
    } else {
      document.getElementById('error_password').style.display = 'none'
      document.getElementById('submit_button').disabled = false
    }
  })

</script>
@endsection
