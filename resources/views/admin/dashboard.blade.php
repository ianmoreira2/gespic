@extends('layouts.app')
@section('title','Dashboard')
@push('header-scripts-aux')
    <link rel="stylesheet" href="{{ asset('css/style.css')}}">

@endpush
@section('content')
<div class="component">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        @if(session('success'))
        <div class="alert alert-success">
           {{ session('success') }}
        </div> 
      @endif
    <h3>Quartos</h3>
    <div id="card-room" style="display: grid; grid-template-columns: repeat(auto-fill, 200px); grid-gap: 1rem">
        @foreach ($rooms as $item)
            <div class="card" onclick='modalFunction({{ $item }})' style="width: 18rem; padding-top: 1rem; cursor: pointer;">
                <div class="card-body @if($item->status == 1) btn-warning @else btn-success @endif" style="border: 1px solid #ccc; padding: 1.25rem; border-radius: .5rem">
                    <h3 class="card-title text-center">Quarto {{ $item->name}}</h3>
                    <h5 class="card-subtitle mb-2 text-muted">Ramal: {{ $item->ramal }}</h5>
                    <h5 class="card-subtitle mb-2 text-muted">@if(isset($item->users[0]))Data de saída: {{$item->users[0]->data_saida }}@endif.</h5>
                   
                </div>
            </div>    
        @endforeach
    </div>
</div>
@include('admin.modalShow')
<script>
    async function modalFunction(obj) {
        if(obj.status == 0) {
            alert('Atenção não há hospedes neste quarto')
        } else {
            var html = ''
            for (let index = obj.users[0].photos.length -1; index >= 0 ; index--) {
                html += ' <div class="card col col-md-4 col-sm-6 col-lg-3">'
                html += `<img src="{{ asset('images/${obj.users[0].photos[index].filename}') }}" />`
                html += `<p>${obj.users[0].photos[index].name}</p>`
                html += '</div>'
            }
            // console.log(html)
            $("#img_card").html(html)
            $('#room_id').html("Quarto Nº "+obj.id)
            $('#room').val(obj.id)
            $('#room_name').html("Nome do quarto: "+obj.name)
            $('#modal-quarto').modal()
        }
    }

    $(document).ready(function() {

        $(".btn-success").click(function(){ 
            var html = $(".clone").html();
            $(".increment").after(html);
        });
  
        $("body").on("click",".btn-danger",function(){ 
            $(this).parents(".control-group").remove();
        });
  
      });
  

</script>
@endsection
