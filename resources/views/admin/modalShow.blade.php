<div id="modal-quarto" class="modal fade bd-example-modal-lg" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
        <div class="modal-title col-md-12 col-sm-12 col-xs-12">
          
        </div>
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
              <div class="dashboard_graph">
                <div class="row x_title">
                 
                  <div class="col-md-6">
                    <h4 id="room_id"></h4>
                  </div>
                </div>
                
                <div class="x_content">
                  <h4 id="room_name"></h4>
                  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident nam quia aut nobis facere quisquam consequuntur ullam odit nulla quasi fuga at alias est eligendi, ducimus magnam asperiores. Pariatur, officia.</p>
                  
                  {{-- <img src="{{ asset('') }}" alt=""> --}}
                  <form method="post" action="{{url('/admin/photo/store')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    
                          <input type="text" style="display: none" id="room" name="room">
                          <div class="input-group control-group increment" >
                            <input type="file" name="filename[]" class="form-control">
                            <div class="input-group-btn"> 
                              <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button>
                            </div>
                          </div>
                          <div class="clone hide">
                            <div class="control-group input-group" style="margin-top:10px">
                              <input type="file" name="filename[]" class="form-control">
                              <div class="input-group-btn"> 
                                <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                              </div>
                            </div>
                          </div>
                  
                          <button type="submit" class="btn btn-primary" style="margin-top:10px">Submit</button>
                    </form>  
                    <div id="photoRoom">
                      <div id="img_card" class="cards row">
                    </div>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>