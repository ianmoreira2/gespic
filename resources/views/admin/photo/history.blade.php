@extends('layouts.app')
@section('title','Histórico')

@section('content')
<div class="component">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        @if(session('success'))
        <div class="alert alert-success">
           {{ session('success') }}
        </div> 
      @endif
    <h3>Histórico de fotos por usuário</h3>
   
</div>
<script>
   
</script>
@endsection
