@extends('layouts.app')
@section('title','Criar Quarto')
@section('content')
<div class="component">
  <h3>Criar Quarto</h3>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        {{-- <div class="x_title"></div> --}}
        <div class="x_content">
          <form action="{{ route('room.store') }}" method="POST" class="form-horizontal form-label-left">
             <input name="_token" id="token" type="hidden" value="{{ csrf_token() }}"/>
            <div class="form-group">
              <label for="" class="control-label col-md-3 col-sm-3 col-xs-12">Nome do quarto</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" class="form-control col-md-7 col-xs-12" required name="name_room" id="name_room">
              </div>
            </div>
            <div class="form-group">
              <label for="" class="control-label col-md-3 col-sm-3 col-xs-12">Ramal</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="phone" data-mask="'(99) 9 9999-9999'" class="form-control col-md-7 col-xs-12" required name="phone_room" id="phone_room">
              </div>
            </div>
            {{-- <div class="ln_solid"></div> --}}
            <div class="form-group">
              <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                <button type="submit" class="btn btn-success">Cadastrar</button>
                <button type="button" class="btn btn-primary">Cancelar</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script>

</script>
@endsection
