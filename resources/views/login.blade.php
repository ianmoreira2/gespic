@extends('layouts.auth')
{{--  @section('title','Login')  --}}
@push('header-scripts-aux')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('css/home.css') }}">
@endpush
@section('content')
<div id="homeView">
    <div id="demo" class="carousel slide" data-ride="carousel">
        <ul class="carousel-indicators">
            <li data-target="#demo" data-slide-to="0" class="active"></li>
            <li data-target="#demo" data-slide-to="1"></li>
            <li data-target="#demo" data-slide-to="2"></li>
        </ul>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="https://www.familyvacationcritic.com/wp-content/uploads/sites/19/2017/01/01bcc97d1362ac3f7cd397e4f3eebd23.jpg"
                    alt="Los Angeles">
                <div class="carousel-caption">
                    <h3>Los Angeles</h3>
                    <p>We had such a great time in LA!</p>
                </div>
            </div>
            <div class="carousel-item">
                <img src="https://abrilviagemeturismo.files.wordpress.com/2016/10/01371480_6h4i4i5f8a-1.jpeg"
                    alt="Chicago">
                <div class="carousel-caption">
                    <h3>Chicago</h3>
                    <p>Thank you, Chicago!</p>
                </div>
            </div>
            <div class="carousel-item">
                <img src="https://magazine.zarpo.com.br/wp-content/uploads/2016/08/Premios-Zarpo-2016-os-5-melhores-hoteis-para-viajar-com-a-familia-900x450.jpg"
                    alt="New York">
                <div class="carousel-caption">
                    <h3>New York</h3>
                    <p>We love the Big Apple!</p>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#demo" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#demo" data-slide="next" style="right:35% ;">
            <span class="carousel-control-next-icon"></span>
        </a>
    </div>
    <div style="width: 40%;height: 100%; background: rgba(0,0,0,0.15); position: absolute; right: 0; top:0; padding: 2rem">
        <section class="login_content">
            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                <ul id="myTabs" class="nav nav-tabs bar_tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Login</a>
                    </li>
                    <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Registrar</a>
                    </li>
                </ul>
                <div id="myTabContent" class="tab-content">
                    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                        {{-- <select name="" id="selectUser" style="width: 100%; height: 40px; font-size: 16px; padding: 1rem;">
                            <option value="">Que tipo de usuário é você?</option>
                            <option value="1">Administrador</option>
                            <option value="2">Hóspede</option>
                        </select> --}}

                        {{-- Form admin --}}
                        <form id="formAdmin" role="form" method="POST" action="{{ url('/login') }}">
                            {{csrf_field()}}
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input type="email" class="form-control" name="email" placeholder="email" required="" />
                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <input type="password" class="form-control" name="password" placeholder="Password"
                                    required="" />
                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div>
                                <button class="btn btn-default submit" type="submit">Log in</button>
                                <a class="reset_pass" href="{{ url('/password/reset') }}" style="color: white">Lost your password?</a>
                            </div>

                            <div class="clearfix"></div>

                            <div class="separator">
                                <p class="change_link" style="color: white">New to site?
                                    <a href="{{route('register')}}" class="to_register" style="color: white"> Create Account </a>
                                </p>

                                <div class="clearfix"></div>
                                <br />

                                <div>
                                    <h1 style="color: white"><i class="fa fa-camera"></i> GESPIC</h1>
                                    <p style="color: white">©{{date('Y')}} </p>
                                </div>
                            </div>
                        </form>

                        {{-- Form hospede --}}
                        {{-- <form id="formHospede" style="display: none" role="form" method="POST" action="{{ url('/login_hospede') }}">
                            {{csrf_field()}}
                            <div class="form-group">
                                <input type="email" placeholder="email@email.com" id="email_guest" name="email_guest" style="height: 35px; font-size: 16px; padding: 1rem">
                            </div>
                            <div class="form-group">
                                <input type="text" placeholder="Token do Quarto" name="token_room" style="height: 35px; font-size: 16px; padding: 1rem">
                            </div>
                            <div>
                                <button class="btn btn-default submit" type="submit">Log in</button>
                                <a class="reset_pass" href="{{ url('/password/reset') }}" style="color: white">Lost your password?</a>
                            </div>
                            <div class="separator">
                                <p class="change_link" style="color: white">New to site?
                                    <a href="{{route('register')}}" class="to_register" style="color: white"> Create Account </a>
                                </p>

                                <div class="clearfix"></div>
                                <br />

                                <div>
                                    <h1 style="color: white"><i class="fa fa-camera"></i> GESPIC</h1>
                                    <p style="color: white">©{{date('Y')}} </p>
                                </div>
                            </div>
                        </form> --}}
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                        <form role="form" method="POST" action="{{ url('/register') }}">
                            {{  csrf_field()}}
                            <div class="form-group"><x></x>
                                <input type="text" class="form-control" name="name" placeholder="Nome completo" required="" />
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="user_name" placeholder="Nome de usuário" required="" />
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="Email" required="" />
                            </div>
                            <div class="form-group">
                                <input type="phone" class="form-control" name="phone" placeholder="Contato" required="" />
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="Senha" required="" />
                            </div>
                    
                            <div class="form-group">
                                <input type="password" class="form-control" name="password_confirmation" placeholder="Confirmação da senha"
                                    required="" />
                            </div>
                            <div>
                                <button class="btn btn-default submit" type="submit">Cadastrar</button>
                                <a class="reset_pass" href="{{ url('/password/reset') }}">Esqueceu sua senha?</a>
                            </div>
                    
                            <div class="clearfix"></div>
                    
                            <div class="separator">
                                <p class="change_link">Já tem uma conta?
                                    <a href="{{route('login')}}" class="to_register">Cadastrar </a>
                                </p>
                    
                                <div class="clearfix"></div>
                                <br />
                    
                                <div><span class="glyphicon glyphicon-camera" aria-hidden="true"></span>
                                    <h1><i class="fa fa-plus-circle"></i> {{config('app.name')}}</h1>
                                    <p>©{{date('Y')}} </p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<script>
    $('#selectUser').change(function() {
        console.log('here')
        let value = $(this).val()
        console.log(value)
        if (value == 1) {
            document.getElementById('formAdmin').style.display = 'block'
            document.getElementById('formHospede').style.display = 'none'
        } else if (value == 2) {
            document.getElementById('formAdmin').style.display = 'none'
            document.getElementById('formHospede').style.display = 'block'
        }
    })
</script>
@endsection